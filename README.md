# Youtube video Scrap project

## Instruction to run project in local machine

#### Let assume the user running ubuntu as their OS

* Clone this repository
* Create a python virtual environment by this command `python3 -m venv venv` 
* Turn on virtual environment `source venv/bin/activate`
* Install all packages, run `pip install -r requirements.txt`
* Install `Redis` in your machine and check if it is working by this command `redis-cli ping`
Here is a good tutorial on Redis installation https://www.digitalocean.com/community/tutorials/how-to-install-and-secure-redis-on-ubuntu-18-04
  
* Create a database in PostgreSQL
* Open the settings.json file and set DB information with user, password
* To run this application, you must have to create an Google APY key and set it to settings.json file
* Run `python manage.py migrate` & `python manage.py createsuper`
* In terminal run `python manage.py scrape_videos`, this command will fetch all videos from a channel which ID has been hard coded
* in Django admin, channel data, all videos of this channel, videos statistics can be found
* To run test cases run the command `python manage.py test scrapper.tests`


## Celery commands
Open two tab in terminal and turn on virtual environment and the in two terminal run the below two commands to start celery

* `celery -A project_test worker -l info`
* `celery -A project_test beat -l info`

## API Endpoint:

``
GET "/api/v1/youtube-videos/"
``


### Response 
```
{
    "count": 2,
    "next": null,
    "previous": null,
    "results": [
        {
        "id": 1301,
        "title": "Coke Studio Sessions: Katy Perry",
        "video_id": "FzscYceVGVo",
        "published_at": "2020-06-27T02:57:36Z"
        }, 
        {
        "id": 1356,
        "title": "Katy Perry - Making of \"Small Talk\" / Episode #4",
        "video_id": "UYfCPvoQgZo",
        "published_at": "2019-09-04T21:36:30Z"
        }
    ]
}
```

Data can be filtered by title/tag, to do follow this

`GET "/api/v1/youtube-videos?title=&tag="`

Response data will be same as above json

## Tracking video statistics after N time

I used celery to update videos statistics. Here we need to be very judgemental. For now let we have N videos where
N is the 32 bit signed integer & we have only one server which are serving all things. Also user are always watching videos, so the statistics is updating very frequently. First of all we need to take a 
decision that are we going to show user very update data from this system or a single user may be there 2-3 or max 10 times here. To be simple lets take that a user will be there 3 times in a day, so we should do something so that on average a user can view update data.

For worst case if we have very large N, then it will take long time to refresh video data each time. But for N (32 bit sign integer), we can set the interval 60min.With this interval if a User visit one hour after, they will be able to see updated data. But if the N increases one by one this process will not work anymore. If then we want same
efficiency we should follow `divide & conquer` methodology. We can create a batch & divide it and then refresh data from both side in a same time.Which means that in t/2 time all N videos will be refreshed. 

In production, AWS lambda would be a good solution for refreshing data for a very BIG N.


## Pseudo algorithm for fetching as many Youtube channels videos

Before loading channel videos data as many as possible, we need to know how the YouTube API works.
A channel id need to pass to channel API which return the playlist id where all videos can be found for that channel. By this playlist if we can get video list of
that channel. But to get more detail information of a video we also need to hit the Youtube video API.

So generally the process is:
* Hit Youtube channel API with a channel id, return a playlist id
* Hit Youtube playlist API with playlist id, return list of channel videos
* For each video of above list, hit Youtube video API with video id to get detail information of a video

So total API hit count for a channel id which contain q videos where q < 50: 1 + 1 + q = q + 2
If q > 50:
```
total_loop = math.ceil(q/per_page_item)
total_count = 1 + total_loop + q
```

No suppose we have a list of channel ids where length of channel ids is P, which all video information we want to store where q > 50. Let's calculate now
```buildoutcfg
total_count = P * (1+ total_loop + 1)
```
Steps of store data:
```
> for channel_id in channels
> Create instance of SystemService class
> Call run_service method of SystemService class instance & pass channel_id
```
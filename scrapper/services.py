from project_test.settings import GOOGLE_AUTH_KEY
import requests
import logging
import math
from django.db import transaction

from scrapper.models import YoutubeChannel, VideoStatistics, VideoTag, YoutubeVideo

logger = logging.getLogger(__name__)


class YoutubeScrapper:
    def __init__(self):
        self.key = GOOGLE_AUTH_KEY
        self.channel_base_url = "https://www.googleapis.com/youtube/v3/channels"
        self.playlist_base_url = "https://www.googleapis.com/youtube/v3/playlistItems"
        self.video_base_url = "https://www.googleapis.com/youtube/v3/videos"
        self.per_page_item = 50

    def __request_data(self, url, params={}):
        """
        This private method is responsible for requesting url and return response data
        """
        try:
            logger.info("Requesting to url")
            response = requests.get(url, params=params)
            return response.json(), response.status_code
        except Exception as e:
            logger.critical("Error: " + str(e))
            return {}, 500

    def request_data(self, url, params):
        return self.__request_data(url, params)

    def get_channel_data(self, channel_id):
        """
        This method is responsible to retrieve playlist id from a youtube channel
        """
        params = {
            "part": "statistics,contentDetails",
            "id": channel_id,
            "key": self.key
        }
        try:
            logger.info("Started requesting to youtube API for playlist id")
            response_data, status_code = self.__request_data(self.channel_base_url, params)
            return (response_data, status_code)
        except Exception as e:
            logger.critical("__get_channel_playlist_id")
            logger.critical(str(e))
            return None, 500

    def get_single_video_info(self, video_id):
        """
        This method is responsible to retrieve single video details
        """
        params = {
            "part": "contentDetails,liveStreamingDetails,id,localizations,snippet,statistics",
            "id": video_id,
            "key": self.key
        }
        logger.critical("Started requesting to youtube API for video list from a youtube channel")
        try:
            response_data, status_code = self.__request_data(self.video_base_url, params)
            return response_data
        except Exception as e:
            logger.critical("get_playlist_videos")
            logger.critical(str(e))
            return None

    def get_playlist_videos(self, playlist_id, max_results):
        """
        This method is responsible to retrieve all videos from a youtube channel
        """
        next_page_token = None
        total_loop = math.ceil(max_results/self.per_page_item)
        total = 0
        params = {
            "part": "id,snippet,ContentDetails",
            "maxResults": max_results,
            "playlistId": playlist_id,
            "key": self.key
        }
        logger.critical("Started requesting to youtube API for video list from a youtube channel")
        video_list = []
        for index in range(total_loop):
            if next_page_token is not None:
                params["pageToken"] = next_page_token
            try:
                response_data, status_code = self.__request_data(self.playlist_base_url, params)
                if status_code >=400:
                    return response_data, status_code
                if response_data:
                    items = response_data["items"]
                    for item in items:
                        video_id = item["contentDetails"]["videoId"]
                        video_list.append(self.get_single_video_info(video_id))
                    logger.info("Requesting completed!")
                    if index < total_loop-1:
                        next_page_token = response_data["nextPageToken"]
            except Exception as e:
                logger.critical("get_playlist_videos")
                logger.critical(str(e))
                return []

        return video_list, 200


class SystemService:
    """
    This class is responsible to store video & channel data to system database
    """
    def __init__(self):
        self.scrapper = YoutubeScrapper()
        self.reference_type = "CHANNEL_ID"

    def __get_commong_stat_payload(self, video, video_data):
        statistics_payload = {
            "video": video,
            "views": int(video_data["items"][0]["statistics"]['viewCount']) if "viewCount" in video_data["items"][0]["statistics"] else 0,
            "likes": int(video_data["items"][0]["statistics"]['likeCount']) if "likeCount" in video_data["items"][0]["statistics"] else 0,
            "dislikes": int(video_data["items"][0]["statistics"]['dislikeCount']) if "dislikeCount" in video_data["items"][0]["statistics"] else 0,
            "comments": int(video_data["items"][0]["statistics"]['commentCount']) if "commentCount" in video_data["items"][0]["statistics"] else 0,
        }

        return statistics_payload

    def __create_video_statistics(self, video, video_data):
        statistics_payload = self.__get_commong_stat_payload(video, video_data)

        instance = VideoStatistics.objects.create(**statistics_payload)
        return instance

    def __update_video_statistics(self, video, video_data, statistics_instance):
        statistics_payload = self.__get_commong_stat_payload(video, video_data)

        statistics_instance.views = statistics_payload["views"]
        statistics_instance.likes = statistics_payload["likes"]
        statistics_instance.dislikes = statistics_payload["dislikes"]
        statistics_instance.comments = statistics_payload["comments"]

        statistics_instance.save()

        return statistics_instance

    def store_channel_data(self, channel_id):
        reference_type = "CHANNEL_ID"
        response, status_code = self.scrapper.get_channel_data(channel_id)
        if status_code == 403:
            logger.critical("Sorry currently you're not able to use this youtube API, status: {0}".format("403"))
            return None

        if status_code >= 400:
            logger.critical("Sorry there are some technical faults, status: {0}".format("403"))
            return None
        logger.critical("API Call finished")
        payload = {
            "reference": channel_id,
            "playlist_id": response["items"][0]["contentDetails"]['relatedPlaylists'][
                'uploads'],
            "reference_type": reference_type,
            "subscriber": int(response["items"][0]["statistics"]['subscriberCount']),
            "video_count": int(response["items"][0]["statistics"]['videoCount']),
            "api_data": response
        }
        with transaction.atomic():
            instance = YoutubeChannel.get_instance({"reference": channel_id, "reference_type": reference_type})
            if instance is None:
                logger.critical("Instance is creating")
                instance = YoutubeChannel.objects.create(**payload)
            else:
                logger.critical("Instance is found!")
                instance.video_count = int(response["items"][0]["statistics"]['videoCount'])
                instance.subscriber = int(response["items"][0]["statistics"]['subscriberCount'])
                instance.api_data = response
                instance.save()
            return instance

    def store_video_data(self, channel_id):

        instance = YoutubeChannel.get_instance({"reference": channel_id, "reference_type": self.reference_type})
        if instance is None:
            return None
        video_list, status_code = self.scrapper.get_playlist_videos(instance.playlist_id, instance.video_count)
        if status_code == 403:
            logger.critical("Sorry currently you're not able to use this youtube API, status: {0}".format("403"))
            return None

        if status_code >= 400:
            logger.critical("Sorry there are some technical faults, status: {0}".format("403"))
            return None
        logger.critical("API Call finished")

        for video in video_list:
            payload = {
                "channel": instance,
                "video_id": video["items"][0]["id"],
                "title": video["items"][0]["snippet"]['title'],
                "description": video["items"][0]["snippet"]['description'],
                "published_at": video["items"][0]["snippet"]['publishedAt'],
                "api_data": video
            }

            tags = video["items"][0]["snippet"]['tags'] if "tags" in video["items"][0]["snippet"] else []
            tag_query_list = []
            for tag in tags:
                tag_instance, created = VideoTag.objects.get_or_create(tag=tag.title())
                tag_query_list.append(tag_instance.id)

            with transaction.atomic():
                video_instance = YoutubeVideo.get_instance({"channel": instance, "video_id": payload["video_id"]})
                if video_instance is not None:
                    video_instance.title = payload['title']
                    video_instance.description = payload['description']
                    video_instance.save()

                    video_instance.tags.clear()
                    if tag_query_list:
                        video_instance.tags.set(tag_query_list)

                    statistics_instance = VideoStatistics.get_instance({"video": video_instance})
                    if statistics_instance is None:
                        statistics_instance = self.__create_video_statistics(video_instance, video)
                    else:
                        self.__update_video_statistics(video_instance, video, statistics_instance)
                else:
                    video_instance = YoutubeVideo.objects.create(**payload)
                    if tag_query_list:
                        video_instance.tags.set(tag_query_list)
                    statistics_instance = self.__create_video_statistics(video_instance, video)


    def run_service(self, channel_id):
        self.store_channel_data(channel_id)
        self.store_video_data(channel_id)
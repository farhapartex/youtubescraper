from django.core.management.base import BaseCommand, CommandError
from scrapper.management.utils.scrap_channel_data import store_channel_data
from scrapper.management.utils.scrap_video_from_channel import store_video_data


class Command(BaseCommand):
    def handle(self, *args, **options):
        channel_id = "UCYvmuw-JtVrTZQ-7Y4kd63Q"
        """
        this method will fetch channel information & will save to db
        """
        store_channel_data(channel_id)

        """
        this method will fetch all videos from a channel information & will save to db
        """
        store_video_data(channel_id)

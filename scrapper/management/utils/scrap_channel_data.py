from django.db import transaction
from scrapper.services import SystemService
import logging

logger = logging.getLogger(__name__)


def store_channel_data(channel_id):
    service_instance = SystemService()
    service_instance.store_channel_data(channel_id)
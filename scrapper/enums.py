from enum import Enum
from django.utils.translation import gettext_lazy as _
from django.db import models


class ChannelReferenceType(models.TextChoices):
    CHANNEL_ID = "CHANNEL_ID", _("Channel Id")
    USERNAME = "USERNAME", _("Username")

    @classmethod
    def get_all(cls):
        return [
            cls.CHANNEL_ID.value,
            cls.USERNAME.value
        ]
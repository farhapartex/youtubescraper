import requests_mock
import requests
from django.conf import settings
from django.test import TestCase, RequestFactory, override_settings
from faker import Faker
from faker.providers import lorem, person, python, internet
from rest_framework import status
from rest_framework.test import APIClient
from scrapper.services import YoutubeScrapper
from scrapper.tests.factories import *

fake = Faker()
fake.add_provider(lorem)
fake.add_provider(person)
fake.add_provider(python)
fake.add_provider(internet)


class YoutubeVideoListAPIViewTest(TestCase):
    def setUp(self):
        self.client = APIClient()

        self.channel = YoutubeChannelFactory.create()
        self.tag1 = VideoTagFactory.create()
        self.tag2 = VideoTagFactory.create()
        self.tag3 = VideoTagFactory.create()
        self.video1 = YoutubeVideoFactory.create(channel=self.channel)
        self.video2 = YoutubeVideoFactory.create(channel=self.channel)
        self.video1.tags.set([self.tag1.id, self.tag2.id, self.tag3.id])
        self.video2.tags.set([self.tag1.id])
        self.statistics1 = VideoStatisticsFactory.create(video=self.video1)
        self.statistics2 = VideoStatisticsFactory.create(video=self.video2)

        self.url = "/api/v1/youtube-videos/"

    def test_api_valid_method(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_api_invalid_method(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, 405)

    def test_api_list_view(self):
        response = self.client.get(self.url)
        response_data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response_data['results']), 2)

    def test_api_list_view_with_filtering(self):
        print(self.tag1.tag, self.tag2.tag)

        response = self.client.get(self.url, params={"tag": self.tag1.tag})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json()['results']), 2)
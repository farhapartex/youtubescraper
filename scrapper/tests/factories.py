import uuid
import factory
from faker import Faker
from faker.providers import lorem, person, python, misc
from random import randint
from scrapper.models import YoutubeChannel, VideoTag, YoutubeVideo, VideoStatistics

fake = Faker()
fake.add_provider(lorem)
fake.add_provider(person)
fake.add_provider(python)
fake.add_provider(misc)


class YoutubeChannelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = YoutubeChannel

    reference = factory.LazyAttribute(lambda _: fake.unique.name()[:25])
    playlist_id = factory.LazyAttribute(lambda _: fake.unique.name()[:25])
    subscriber = factory.LazyAttribute(lambda _: randint(5, 2000))
    video_count = factory.LazyAttribute(lambda _: randint(5, 2000))
    api_data = {"data": "fake data"}


class VideoTagFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VideoTag

    tag = factory.LazyAttribute(lambda _: fake.unique.name()[:25])


class YoutubeVideoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = YoutubeVideo

    video_id = factory.LazyAttribute(lambda _: fake.unique.name()[:25])
    title = factory.LazyAttribute(lambda _: fake.unique.name()[:25])
    published_at = factory.LazyAttribute(lambda _: fake.date_time())
    api_data = {"data": "fake data"}


class VideoStatisticsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = VideoStatistics

    views = factory.LazyAttribute(lambda _: randint(5, 200000))
    likes = factory.LazyAttribute(lambda _: randint(5, 200000))
    dislikes = factory.LazyAttribute(lambda _: randint(5, 200000))
    comments = factory.LazyAttribute(lambda _: randint(5, 200000))
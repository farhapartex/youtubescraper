import requests_mock
import requests
from django.conf import settings
from django.test import TestCase, RequestFactory, override_settings
from faker import Faker
from faker.providers import lorem, person, python, internet
from rest_framework import status
from scrapper.services import YoutubeScrapper

fake = Faker()
fake.add_provider(lorem)
fake.add_provider(person)
fake.add_provider(python)
fake.add_provider(internet)


class YoutubeScrapperServiceTestCase(TestCase):
    @override_settings(
        GOOGLE_AUTH_KEY='api_key'
    )
    def test_channel_data(self):

        channel_id = "TEST_CHANNELID0078"
        response_body = {
            "kind": "youtube#channelListResponse",
            "etag": "ZVsol5tjX8EQgWmIFxGdv1iw",
            "pageInfo": {
                "totalResults": 1,
                "resultsPerPage": 5
            },
            "items": [
                {
                    "kind": "youtube#channel",
                    "etag": "Y2cqSQ05lMo3hCxJj6u1r42g",
                    "id": channel_id,
                    "contentDetails": {
                        "relatedPlaylists": {
                            "likes": "",
                            "favorites": "",
                            "uploads": "TEST_ID0089"
                        }
                    },
                    "statistics": {
                        "viewCount": "29400968",
                        "subscriberCount": "186000",
                        "hiddenSubscriberCount": False,
                        "videoCount": "200"
                    }
                }
            ]
        }
        params = {
            "part": "statistics,contentDetails",
            "id": channel_id,
            "key": settings.GOOGLE_AUTH_KEY
        }

        with requests_mock.Mocker() as m:
            scrapper = YoutubeScrapper()
            url = "https://example.com"
            m.get(url, json=response_body, status_code=200)
            response, status_code = scrapper.request_data(url, params)
            self.assertEqual(status_code, 200)
            self.assertEqual(response['items'][0]['id'], channel_id)
            self.assertEqual(response['items'][0]['statistics']['viewCount'], "29400968")

    @override_settings(
        GOOGLE_AUTH_KEY='api_key'
    )
    def test_playlist_videos_data(self):
        response_body = {
            "kind": "youtube#playlistItemListResponse",
            "etag": "uB-cUvG7wmNpTn4vGPodutjR1VU",
            "nextPageToken": "",
            "items": [
                {
                    "kind": "youtube#playlistItem",
                    "etag": "4lmfZSPTzrrTy_Gq2rrypBnvx6s",
                    "id": "VVVFWF9YQTBoMG9Qa2ZlR2FwaUR4MTd3LlVoOXJTbWRMeVZN",
                    "snippet": {
                        "publishedAt": "2021-05-28T09:49:41Z",
                        "channelId": "UCEX_XA0h0oPkfeGapiDx17w",
                        "title": "Radhe: Your Most Wanted Bhai Movie | Salman Khan's Family Deleted Scene | Nancy Jain | Zarina Wahab.",
                        "description": "Megastar Salman Khan's Biggest Released Radhe Movie Deleted Scene Related Important Details Out....\n\n\n\n\nFreinds Video Accha Laga Hoto Like Kre Aur Channel Ko Subscribe Karke Bell Icon Ko Dabana Na Bhule Video Dekhne Ke Liye Aapka Dilse Thanks....\n\n\n\n\n#SalmanKhan",
                        "thumbnails": {
                            "default": {
                                "url": "https://i.ytimg.com/vi/Uh9rSmdLyVM/default.jpg",
                                "width": 120,
                                "height": 90
                            }
                        },
                        "channelTitle": "MF News",
                        "playlistId": "UUEX_XA0h0oPkfeGapiDx17w",
                        "position": 0,
                        "resourceId": {
                            "kind": "youtube#video",
                            "videoId": "Uh9rSmdLyVM"
                        },
                        "videoOwnerChannelTitle": "MF News",
                        "videoOwnerChannelId": "UCEX_XA0h0oPkfeGapiDx17w"
                    },
                    "contentDetails": {
                        "videoId": "Uh9rSmdLyVM",
                        "videoPublishedAt": "2021-05-28T09:55:56Z"
                    }
                }
            ]
        }

        params = {
            "part": "statistics,contentDetails",
            "maxResults": 50,
            "playlistId": "test_playlist_id_009",
            "key": settings.GOOGLE_AUTH_KEY
        }

        with requests_mock.Mocker() as m:
            scrapper = YoutubeScrapper()
            url = "https://example.com"
            m.get(url, json=response_body, status_code=200)
            response, status_code = scrapper.request_data(url, params)
            self.assertEqual(status_code, 200)
            self.assertEqual(len(response['items']), 1)
            self.assertEqual(len(response['nextPageToken']), 0)

    @override_settings(
        GOOGLE_AUTH_KEY='api_key'
    )
    def test_video_details_data(self):
        response_body = {
            "kind": "youtube#videoListResponse",
            "etag": "_LwmCBmV36LFkj0xaGe0XpHBg54",
            "items": [
                {
                    "kind": "youtube#video",
                    "etag": "BWWd60-3rgAk-8gOMucQb2MNsag",
                    "id": "0KSOMA3QBU0",
                    "snippet": {
                        "publishedAt": "2014-02-20T20:00:03Z",
                        "channelId": "UC-8Q-hLdECwQmaWNwXitYDw",
                        "title": "Katy Perry - Dark Horse (Official) ft. Juicy J",
                        "description": "Listen to Katy’s new song “Smile”: https://katy.to/smileID\n\nGet \"Dark Horse\" from Katy Perry's 'PRISM': http://katy.to/PRISM\n\nKaty Perry Complete Collection on Spotify: http://katy.to/SpotifyCompleteYD\nKaty Perry Essentials on Apple Music: http://katy.to/AMEssentialsYD\nWatch your favorite Katy videos on YouTube: http://katy.to/MusicVideosYD\n\nFollow Katy Perry:\nWebsite: http://katy.to/WebsiteYD\nInstagram: http://katy.to/InstagramYD\nTwitter: http://katy.to/TwitterYD\nFacebook: http://katy.to/FacebookYD\n\n\nDirected by Matthew Cullen & Produced by Dawn Rose, Danny Lockwood, Javier Jimenez, and Derek Johnson\n\n\nLyrics:\nI knew you were\nYou were gonna come to me\nAnd here you are\nBut you better choose carefully\n‘Cause I am capable of anything\nOf anything and everything\nMake me your Aphrodite\nMake me your one and only\nBut don’t make me your enemy\nYour enemy, your enemy\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\nMark my words\nThis love will make you levitate\nLike a bird\nLike a bird without a cage\nBut down to earth\nIf you choose to walk away\nDon’t walk away\nIt’s in the palm of your hand now, baby\nIt’s a yes or a no, no maybe\nSo just be sure\nBefore you give it all to me\nAll to me\nGive it all to me\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\n(Juicy J)\nShe’s a beast\nI call her Karma\nShe’ll eat your heart out\nLike Jeffrey Dahmer\n\nBe careful\nTry not to lead her on\nShorty heart is on steroids\n‘Cause her love is so strong\n\nYou may fall in love when you meet her\nIf you get the chance, you better keep her\nShe’s sweet as pie, but if you break her heart\nShe’ll turn cold as a freezer\n\nThat fairy tale ending with a knight in shining armor\nShe can be my Sleeping Beauty\nI’m gon’ put her in a coma\n\nNow I think I love her\nShorty so bad, sprung and I don’t care\nShe ride me like a roller coaster\nTurned the bedroom into a fair\n\nHer love is like a drug\nI was tryna hit it and quit it\nBut lil’ mama so dope\nI messed around and got addicted\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\n\nMusic video by Katy Perry performing Dark Horse. (C) 2014 Capitol Records, LLC",
                        "thumbnails": {
                            "default": {
                                "url": "https://i.ytimg.com/vi/0KSOMA3QBU0/default.jpg",
                                "width": 120,
                                "height": 90
                            }
                        },
                        "channelTitle": "KatyPerryVEVO",
                        "tags": [
                            "Katy perry",
                            "dark horse"
                        ],
                        "categoryId": "10",
                        "liveBroadcastContent": "none",
                        "localized": {
                            "title": "Katy Perry - Dark Horse (Official) ft. Juicy J",
                            "description": "Listen to Katy’s new song “Smile”: https://katy.to/smileID\n\nGet \"Dark Horse\" from Katy Perry's 'PRISM': http://katy.to/PRISM\n\nKaty Perry Complete Collection on Spotify: http://katy.to/SpotifyCompleteYD\nKaty Perry Essentials on Apple Music: http://katy.to/AMEssentialsYD\nWatch your favorite Katy videos on YouTube: http://katy.to/MusicVideosYD\n\nFollow Katy Perry:\nWebsite: http://katy.to/WebsiteYD\nInstagram: http://katy.to/InstagramYD\nTwitter: http://katy.to/TwitterYD\nFacebook: http://katy.to/FacebookYD\n\n\nDirected by Matthew Cullen & Produced by Dawn Rose, Danny Lockwood, Javier Jimenez, and Derek Johnson\n\n\nLyrics:\nI knew you were\nYou were gonna come to me\nAnd here you are\nBut you better choose carefully\n‘Cause I am capable of anything\nOf anything and everything\nMake me your Aphrodite\nMake me your one and only\nBut don’t make me your enemy\nYour enemy, your enemy\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\nMark my words\nThis love will make you levitate\nLike a bird\nLike a bird without a cage\nBut down to earth\nIf you choose to walk away\nDon’t walk away\nIt’s in the palm of your hand now, baby\nIt’s a yes or a no, no maybe\nSo just be sure\nBefore you give it all to me\nAll to me\nGive it all to me\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\n(Juicy J)\nShe’s a beast\nI call her Karma\nShe’ll eat your heart out\nLike Jeffrey Dahmer\n\nBe careful\nTry not to lead her on\nShorty heart is on steroids\n‘Cause her love is so strong\n\nYou may fall in love when you meet her\nIf you get the chance, you better keep her\nShe’s sweet as pie, but if you break her heart\nShe’ll turn cold as a freezer\n\nThat fairy tale ending with a knight in shining armor\nShe can be my Sleeping Beauty\nI’m gon’ put her in a coma\n\nNow I think I love her\nShorty so bad, sprung and I don’t care\nShe ride me like a roller coaster\nTurned the bedroom into a fair\n\nHer love is like a drug\nI was tryna hit it and quit it\nBut lil’ mama so dope\nI messed around and got addicted\n\n(Pre-Chorus)\nSo you wanna play with magic\nBoy, you should know what you’re fallin’ for\nBaby, do you dare to do this\n‘Cause I’m coming atcha like a dark horse\n\n(Chorus)\nAre you ready for, ready for\nA perfect storm, perfect storm\n‘Cause once you’re mine, once you’re mine\nThere’s no going back\n\n\nMusic video by Katy Perry performing Dark Horse. (C) 2014 Capitol Records, LLC"
                        },
                        "defaultAudioLanguage": "en-US"
                    },
                    "contentDetails": {
                        "duration": "PT3M45S",
                        "dimension": "2d",
                        "definition": "hd",
                        "caption": "false",
                        "licensedContent": True,
                        "contentRating": {},
                        "projection": "rectangular"
                    },
                    "statistics": {
                        "viewCount": "3073945716",
                        "likeCount": "13272445",
                        "dislikeCount": "1290917",
                        "favoriteCount": "0",
                        "commentCount": "761018"
                    }
                }
            ],
            "pageInfo": {
                "totalResults": 1,
                "resultsPerPage": 1
            }
        }

        params = {
            "part": "contentDetails,liveStreamingDetails,id,localizations,snippet,statistics",
            "id": "test_video_id_009",
            "key": settings.GOOGLE_AUTH_KEY
        }

        with requests_mock.Mocker() as m:
            scrapper = YoutubeScrapper()
            url = "https://example.com"
            m.get(scrapper.video_base_url, json=response_body, status_code=200)
            response, status_code = scrapper.request_data(scrapper.video_base_url, params)
            self.assertEqual(status_code, 200)
            self.assertEqual(len(response['items']), 1)
            self.assertEqual(response['items'][0]['statistics']['viewCount'], '3073945716')
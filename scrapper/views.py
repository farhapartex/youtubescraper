from django.shortcuts import render
from rest_framework import generics
from django_filters.rest_framework import DjangoFilterBackend

from scrapper.filters import YoutubeVideoFilter
from scrapper.models import YoutubeVideo
from scrapper.serializers import YoutubeVideoSerializer
# Create your views here.


class YoutubeVideoListAPIView(generics.ListAPIView):
    queryset = YoutubeVideo.objects.all()
    serializer_class = YoutubeVideoSerializer
    filter_backends = (DjangoFilterBackend,)
    filterset_class = YoutubeVideoFilter

    def get_queryset(self):
        return YoutubeVideo.objects.all().prefetch_related('tags')

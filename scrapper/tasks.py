from celery.schedules import crontab
from celery.decorators import periodic_task
from celery.utils.log import get_task_logger
from datetime import datetime
from scrapper.services import SystemService
from scrapper.models import YoutubeChannel
import logging

logger = get_task_logger(__name__)
sys_logger = logging.getLogger(__name__)


@periodic_task(
    run_every=(crontab(minute='*/60')),
    # run_every=timedelta(seconds=5),
    name="refresh_video_data_task",
    ignore_result=True
)
def refresh_video_data_task():
    # do something
    print("Tasks started")
    service_instance = SystemService()
    channels = YoutubeChannel.objects.all()

    for channel in channels:
        channel_id = channel.reference
        logger.info("channel info update started")
        service_instance.store_channel_data(channel_id)
        logger.info("channel info update finished")
        logger.info("channel videos info update started")
        service_instance.store_video_data(channel_id)
        logger.info("channel videos info update finished")
    print("Tasks finished")
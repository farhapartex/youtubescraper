from django_filters import rest_framework as filters
from django.db.models import Q, Subquery
import logging

from scrapper.models import VideoStatistics

logger = logging.getLogger(__name__)


class YoutubeVideoFilter(filters.FilterSet):
    title = filters.CharFilter(method="filter_by_title")
    tag = filters.CharFilter(method="filter_by_tag")

    def filter_by_title(self, queryset, name, value):
        if value is None:
            return queryset
        try:
            return queryset.filter(title__contains=value)
        except:
            return queryset.none()

    def filter_by_tag(self, queryset, name, value):
        if value is None:
            return queryset
        try:
            return queryset.filter(tags__tag__contains=value)
        except:
            return queryset.none()
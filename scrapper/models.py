from django.db import models

from scrapper.enums import ChannelReferenceType


class BaseAbstractEntity(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @classmethod
    def get_instance(cls, filter_criterias):
        instance = cls.objects.filter(**filter_criterias).first()
        return instance

    @classmethod
    def get_filter_data(cls, filter_criterias):
        queryset = cls.objects.filter(**filter_criterias)
        return queryset


class YoutubeChannel(BaseAbstractEntity):
    reference = models.CharField(max_length=200)
    playlist_id = models.CharField(max_length=250, null=True)
    reference_type = models.CharField(max_length=20, choices=ChannelReferenceType.choices, default=ChannelReferenceType.CHANNEL_ID.value)
    subscriber = models.IntegerField(default=0)
    video_count = models.IntegerField(default=0)
    api_data = models.JSONField()

    def __str__(self):
        return self.reference


class VideoTag(BaseAbstractEntity):
    tag = models.CharField(max_length=200)

    def __str__(self):
        return self.tag


class YoutubeVideo(BaseAbstractEntity):
    channel = models.ForeignKey(YoutubeChannel, related_name="videos", on_delete=models.CASCADE)
    video_id = models.CharField(max_length=255, null=True)
    title = models.CharField(max_length=250)
    tags = models.ManyToManyField(VideoTag, blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    published_at = models.DateTimeField()
    api_data = models.JSONField()

    def __str__(self):
        return self.title


class VideoStatistics(BaseAbstractEntity):
    video = models.OneToOneField(YoutubeVideo, related_name="statistics", on_delete=models.CASCADE)
    views = models.IntegerField()
    likes = models.IntegerField()
    dislikes = models.IntegerField()
    comments = models.IntegerField()

    def __str__(self):
        return str(self.id)
from django.urls import path, include, re_path

from scrapper.views import YoutubeVideoListAPIView

urlpatterns = [
    path("api/v1/youtube-videos/", YoutubeVideoListAPIView.as_view(),name="youtube_videos"),
]
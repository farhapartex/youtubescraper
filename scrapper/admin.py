from django.contrib import admin
from scrapper.models import *


# Register your models here.

@admin.register(YoutubeChannel)
class YoutubeChannelAdmin(admin.ModelAdmin):
    list_display = ("id", "reference", "reference_type", "video_count")


@admin.register(VideoTag)
class VideoTagAdmin(admin.ModelAdmin):
    list_display = ("id", "tag")


@admin.register(YoutubeVideo)
class YoutubeVideoAdmin(admin.ModelAdmin):
    list_display = ("id", "channel", "title", "published_at")


@admin.register(VideoStatistics)
class VideoStatisticsAdmin(admin.ModelAdmin):
    list_display = ("id", "video", "views", "likes", "dislikes", "comments")

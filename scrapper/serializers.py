from rest_framework import serializers
from scrapper.models import YoutubeChannel, YoutubeVideo, VideoTag


class VideoTagSerializer(serializers.ModelSerializer):
    class Meta:
        model = VideoTag
        fields = ("id", "tag")


class YoutubeVideoSerializer(serializers.ModelSerializer):

    class Meta:
        model = YoutubeVideo
        fields = ("id", "title", "video_id", "published_at")